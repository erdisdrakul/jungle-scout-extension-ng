import { Injectable } from '@angular/core';
import ProductModel from '../models/product.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SearchProductsService {
    constructor(private httpClient: HttpClient) { }

    async search(query: string, page: number): Promise<ProductModel[]> {
        const products = this.httpClient.get<ProductModel[]>(`${ environment.apiUrl }/search?k=${ query }&page=${ page }`).toPromise();
        return products;
    }
}
