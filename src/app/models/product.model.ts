export default interface ProductModel {
    asin: string;
    name: string;
    brand: string;
    price: number;
    category: string;
    rank: number;
    monthlySales: number;
    dailySales: number;
    revenue: number;
    reviews: number;
    seller: string;
    imageUrl: string;
    description: string;
    stars: number;
}
