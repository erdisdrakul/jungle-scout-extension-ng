import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PriceHistoryDialogComponent } from './components/price-history-dialog/price-history-dialog.component';
import { SearchProductsService } from './services/search-products.service';

@NgModule({
    declarations: [
        AppComponent,
        PriceHistoryDialogComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule
    ],
    providers: [
        SearchProductsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
