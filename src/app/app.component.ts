import { Component, OnInit } from '@angular/core';
import { SearchProductsService } from './services/search-products.service';
import ProductModel from './models/product.model';

interface ProductVm extends ProductModel {
    popoverContent: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    products: ProductVm[] = [];
    averageDailySales: number;
    averageSalesRank: number;
    averagePrice: number;
    averageReviews: number;
    searchText: string;
    state: 'none' | 'loading' = 'none';
    page: number;

    constructor(private searchProductsService: SearchProductsService) {
        this.page = 1;
    }

    ngOnInit(): void {
    }

    onSearchEnter() {
        this.page = 1;
        this.searchProducts(this.searchText, this.page);
    }

    onClearSearchClick() {
        this.products = [];
        this.searchText = null;
        this.averageDailySales = null;
        this.averageSalesRank = null;
        this.averagePrice = null;
        this.averageReviews = null;
    }

    onNextPageClick() {
        this.page += 1;
        this.searchProducts(this.searchText, this.page);
    }

    private async searchProducts(keywords: string, page: number) {
        this.state = 'loading';

        this.products = await this.searchProductsService.search(keywords.replace(' ', '+'), page) as ProductVm[];

        this.averageDailySales = 0;
        this.averageSalesRank = 0;
        this.averagePrice = 0;
        this.averageReviews = 0;

        this.products = this.products.filter(p => p.price != null);

        this.products.forEach(p => {
            this.averageDailySales += p.dailySales
            this.averageSalesRank += p.rank;
            this.averagePrice += p.price;
            this.averageReviews += p.reviews
        });
        this.averageDailySales = Math.round(this.averageDailySales / this.products.length);
        this.averageSalesRank = Math.round(this.averageSalesRank / this.products.length);
        this.averagePrice = this.averagePrice / this.products.length;
        this.averageReviews = Math.round(this.averageReviews / this.products.length);

        this.products.forEach(p => {
            let fillStarsHtml = '';
            let emptyStarsHtml = '';
            for (let j = 0; j < p.stars; ++j) fillStarsHtml += '<i class="fas fa-star mr-1"></i>';
            for (let j = p.stars; j < 5; ++j) emptyStarsHtml += '<i class="fal fa-star mr-1"></i>';

            p.popoverContent = `
<div class="card mb-g shadow-none border-0">
    <div class="card-body pb-0 px-4">
        <div class="d-flex flex-row pb-3 pt-2  border-top-0 border-left-0 border-right-0">
            <div class="d-inline-block align-middle mr-3">
                <img class="d-block" style="max-width: 200px" src="${ p.imageUrl }"/>
            </div>
            <h1 class="mb-0 flex-1 text-dark fw-500">
                <small class="m-0 l-h-n mb-2">
                    ${ p.name }
                </small>
                $${ p.price }
                <div class="pt-2 d-flex text-primary align-items-center">
                    ${ fillStarsHtml }
                    ${ emptyStarsHtml }
                </div>
            </h1>
        </div>
    </div>
</div>
            `
        })

        this.state = 'none';

        setTimeout(_ => {
            $('[data-toggle="popover"]').popover({
                sanitize: false
            });
        }, 300);

    }
}
